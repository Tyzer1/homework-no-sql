﻿using Otus.ReferenceInformation.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.ReferenceInformation.Core.Abstractions.Gateways
{
    public interface IPrefForReceivingFromPartnerGateway
    {
        Task GivePrefToReceivingFormPartner(Preference preference);
    }
}
