﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.ReferenceInformation.Core.Domain
{
    public class Preference
        : BaseEntity
    {
        public string Name { get; set; }
    }
}
