﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Text;
using Otus.ReferenceInformation.Core.Domain;

namespace Otus.ReferenceInformation.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(bc => bc.Customer)
            //    .WithMany(b => b.Preferences)
            //    .HasForeignKey(bc => bc.CustomerId);
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(bc => bc.Preference)
            //    .WithMany()
            //    .HasForeignKey(bc => bc.PreferenceId);
        }
    }
}
