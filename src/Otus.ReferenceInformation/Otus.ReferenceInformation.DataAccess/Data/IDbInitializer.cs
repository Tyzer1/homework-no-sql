﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.ReferenceInformation.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
