﻿using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Linq;
using System;
using Otus.ReferenceInformation.Core.Abstractions.Repositories;
using Otus.ReferenceInformation.Core.Domain;
using System.Collections.Generic;
using Otus.ReferenceInformation.WebHost.Models;
using Microsoft.Extensions.Caching.Distributed;
using System.Threading;
using Newtonsoft.Json;

namespace Otus.ReferenceInformation.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController 
        : ControllerBase
    {
        private readonly IDistributedCache _redis;
        private readonly IRepository<Preference> _preferencesRepository;


        public PreferencesController(IDistributedCache redis,
            IRepository<Preference> preferenceRepository)
        {
            _redis = redis;
            _preferencesRepository = preferenceRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync(CancellationToken cancellationToken = default)
        {
            var key = "preferences";
            var cachedPreferences = await _redis.GetStringAsync(key, cancellationToken);
            IEnumerable<Preference> preferences;
            if (string.IsNullOrEmpty(cachedPreferences))
            {
                preferences = await _preferencesRepository.GetAllAsync();
                if (preferences == null) return NotFound();

                await _redis.SetStringAsync(
                    key,
                    JsonConvert.SerializeObject(preferences),
                    cancellationToken);
                return Ok(GetPreferencesResponce(preferences));
            }
            preferences = JsonConvert.DeserializeObject<IEnumerable<Preference>>(cachedPreferences);

            return Ok(GetPreferencesResponce(preferences));
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var key = id.ToString();
            var cachedPreference = await _redis.GetStringAsync(key, cancellationToken);
            Preference preference;
            if (string.IsNullOrEmpty(cachedPreference))
            {
                preference = await _preferencesRepository.GetByIdAsync(id);
                if (preference == null) return NotFound();

                await _redis.SetStringAsync(
                    key,
                    JsonConvert.SerializeObject(preference),
                    cancellationToken);
                return Ok(GetPreferenceResponse(preference));
            }
            preference = JsonConvert.DeserializeObject<Preference>(cachedPreference);

            return Ok(GetPreferenceResponse(preference));
        }

        private List<PreferenceResponse> GetPreferencesResponce(IEnumerable<Preference> preferences) 
        {
            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            return response;
        }

        private PreferenceResponse GetPreferenceResponse(Preference preference)
        {
            PreferenceResponse response = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
            };
            return response;
        }
    }
}
