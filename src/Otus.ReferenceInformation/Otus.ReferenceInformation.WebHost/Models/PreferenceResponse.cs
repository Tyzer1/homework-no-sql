﻿using System;

namespace Otus.ReferenceInformation.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
