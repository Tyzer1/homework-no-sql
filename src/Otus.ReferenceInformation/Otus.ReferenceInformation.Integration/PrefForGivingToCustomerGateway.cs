﻿using Otus.ReferenceInformation.Core.Abstractions.Gateways;
using Otus.ReferenceInformation.Core.Domain;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.ReferenceInformation.Integration
{
    public class PrefForGivingToCustomerGateway :
        IPrefForGivingToCustomerGateway
    {
        private readonly HttpClient _httpClient;

        public PrefForGivingToCustomerGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task GivePrefToGivingToCustomer(Preference preference)
        {
            var response = await _httpClient.PostAsJsonAsync("api/v1/preferences", preference);

            response.EnsureSuccessStatusCode();
        }
    }
}
