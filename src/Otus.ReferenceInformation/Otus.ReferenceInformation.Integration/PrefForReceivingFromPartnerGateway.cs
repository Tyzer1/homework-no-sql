﻿using Otus.ReferenceInformation.Core.Abstractions.Gateways;
using Otus.ReferenceInformation.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.ReferenceInformation.Integration
{
    public class PrefForReceivingFromPartnerGateway :
        IPrefForReceivingFromPartnerGateway
    {
        private readonly HttpClient _httpClient;

        public PrefForReceivingFromPartnerGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task GivePrefToReceivingFormPartner(Preference preference)
        {
            var response = await _httpClient.PostAsJsonAsync("api/v1/preferences", preference);

            response.EnsureSuccessStatusCode();
        }
    }
}
