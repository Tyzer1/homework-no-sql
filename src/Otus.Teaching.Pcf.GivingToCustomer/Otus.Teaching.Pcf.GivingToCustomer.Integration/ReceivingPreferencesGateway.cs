﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class ReceivingPreferencesGateway :
        IReceivingPreferencesGateway
    {
        private readonly HttpClient _httpClient;

        public ReceivingPreferencesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Preference>> ReceivePreferences()
        {
            var response = await _httpClient.GetStringAsync("api/v1/preferences");
            var preferences = JsonConvert.DeserializeObject<IEnumerable<Preference>>(response);
            return preferences;
        }

        public async Task<Preference> ReceivePreferenceById(Guid id)
        {
            var response = await _httpClient.GetStringAsync($"api/v1/preferences/{id}");
            var preference = JsonConvert.DeserializeObject<Preference>(response);
            return preference;
        }

        public async Task<IEnumerable<Preference>> ReceivePreferencesRangeByIds(List<Guid> ids)
        {
            var response = await _httpClient.GetStringAsync("api/v1/preferences");
            var preferences = JsonConvert.DeserializeObject<IEnumerable<Preference>>(response);
            preferences = preferences.Where(x => ids.Contains(x.Id));
            return preferences;
        }
    }
}
