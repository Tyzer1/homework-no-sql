﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IReceivingPreferencesGateway
    {
        Task<IEnumerable<Preference>> ReceivePreferences();
        Task<Preference> ReceivePreferenceById(Guid id);
        Task<IEnumerable<Preference>> ReceivePreferencesRangeByIds(List<Guid> ids);
    }
}
