﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class ReceivingPreferencesGateway :
        IReceivingPreferencesGateway
    {
        private readonly HttpClient _httpClient;

        public ReceivingPreferencesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Preference>> ReceivePreferences()
        {
            var response = await _httpClient.GetStringAsync("api/v1/preferences");
            var preferences = JsonConvert.DeserializeObject<IEnumerable<Preference>>(response);
            return preferences;
        }

        public async Task<Preference> ReceivePreferenceById(Guid id)
        {
            var response = await _httpClient.GetStringAsync($"api/v1/preferences/{id}");
            var preference = JsonConvert.DeserializeObject<Preference>(response);
            return preference;
        }
    }
}
